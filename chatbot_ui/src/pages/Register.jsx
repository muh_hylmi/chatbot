import React, { useState } from "react";
import Header from "../component/Header";
import "./chat.css";
import logo from "../static/img/logo.jpg";
import { Link } from "react-router-dom";
import { publicRequest } from "../requestMethods";
import { useHistory } from "react-router-dom";
const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [className, setClassName] = useState(
    "alert alert-danger w-75 mr-auto ml-auto alert-invalid hidden"
  );
  const [alert, setAlert] = useState("");
  const history = useHistory();
  const handleSubmit = (e) => {
    e.preventDefault();
    publicRequest
      .post("/register", { name, email, password })
      .then((res) => {
        setClassName("alert alert-success w-75 mr-auto ml-auto alert-invalid");
        setAlert("register success !!");
        history.push("/login");
      })
      .catch((err) => {
        setClassName("alert alert-danger w-75 mr-auto ml-auto alert-invalid");
        setAlert("register failed !!");
      });
  };
  return (
    <div className="container d-flex justify-content-center">
      <div className="card mt-5">
        <Header hide="hide" />
        <img
          src={logo}
          width={150}
          height={150}
          className="img-logo mr-auto ml-auto mt-auto mb-2"
          alt=""
        />
        <div className={className} role="alert">
          {alert}
        </div>
        <div className="container mb-auto container-login">
          <form>
            <div className="form-group">
              <input
                type="text"
                className="form-control form-login"
                placeholder="your name..."
                required={true}
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="form-group">
              <input
                type="email"
                className="form-control form-login"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                placeholder="email..."
                required={true}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="form-group">
              <input
                type="password"
                className="form-control form-login"
                id="exampleInputPassword1"
                placeholder="password..."
                required={true}
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <button
              type="submit"
              className="btn btn-success btn-login"
              onClick={handleSubmit}
            >
              Register
            </button>
          </form>
          <div className="mt-4 text-center">
            <Link to={"/login"}>have account?</Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
