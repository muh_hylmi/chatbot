**Aplikasi chatbot** sederhana dibuat menggunakan **Django** di backend dan **React** di Front end

cara run aplikasi ada di file **command.txt** di masing-masing directory :
1. Run Server django setelah itu
2. Run app client


**cara kerja aplikasi :**
1. user login / register jika belum ada akun
2. setelah login user dapat menanyai bot (pertanyaan masih terbatas menggunakan bahasa indonesia) seperti :
    (halo, kamu siapa, hobi kamu apa, apa kabar, , dll)
3. jika user tidak mengetikan sesuatu 15 detik, otomatis muncul alert
4. jika sudah selesai bisa logout
