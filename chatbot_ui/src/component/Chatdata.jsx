import React from "react";

const Chatdata = (props) => {
  const bot = (
    <div className="d-flex flex-row p-3 ml-auto">
      <div className="bg-white mr-2 p-3">
        <span className="text-muted">{props.message}</span>
      </div>
      <img
        src="https://img.icons8.com/color/48/000000/circled-user-male-skin-type-7.png"
        width={50}
        height={50}
        alt={""}
      />
    </div>
  );
  const user = (
    <div className="d-flex flex-row p-3">
      <img
        src="https://img.icons8.com/color/48/000000/circled-user-female-skin-type-7.png"
        width={50}
        height={50}
        alt=""
      />
      <div className="chat ml-2 p-3">{props.message}</div>
    </div>
  );

  if (props.isBot) {
    return bot;
  } else {
    return user;
  }
};

export default Chatdata;
