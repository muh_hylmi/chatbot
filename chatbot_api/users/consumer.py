import json
from channels.generic.websocket import WebsocketConsumer
import jwt
from chatbot_api.settings import STATIC_URL, STATICFILES_DIRS
from users.chatbot import chatbot
from users.models import User
from rest_framework.exceptions import AuthenticationFailed

class WSConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()
    
    def receive(self, text_data):
        data = json.loads(text_data)
        if data['type'] == 'connect':
            print(data['user']+ " is connected")
        elif data['type'] == 'chat':
            token = data['token']
            message = data['message']
            if not token:
                self.send(json.dumps("Unauthenticated"))
            try:
                payload = jwt.decode(token, 'secret', algorithms=['HS256'])
            except jwt.ExpiredSignatureError:
                self.send(json.dumps("Unauthenticated"))

            user = User.objects.filter(email=payload['email']).first()
            if not user:
                self.send(json.dumps("Unauthenticated"))
            else:
                bot_response = chatbot(message)
                self.send(json.dumps({'token': token, 'response':{'isBot': True, "message": bot_response}}))
        elif data['type'] == 'dissconnect':
            print(data['user']+ " is dissconnected")
        else:
            print("not found type message")