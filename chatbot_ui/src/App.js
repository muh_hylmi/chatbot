import React from 'react'
import Chat from './pages/Chat';
import Login from './pages/Login';
import Register from './pages/Register';
import { BrowserRouter as Router, Redirect, Switch } from 'react-router-dom';
import { Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
const App = () => {
  const token = useSelector((state) => state?.user?.currentUser?.jwt);
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/">{!token ? <Redirect to="/login" /> : <Chat />}</Route>
          <Route exact path="/login">{token ? <Redirect to="/" /> : <Login />}</Route>
          <Route exact path="/register">{token ? <Redirect to="/" /> : <Register />}</Route>

        </Switch>
      </Router>

    </div>

  )
}

export default App;
