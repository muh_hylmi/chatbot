import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../redux/apiCalls";

const Header = (props) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.currentUser?.name);

  const handleClick = () => {
    props.socket.send(
      JSON.stringify({
        type: "dissconnect",
        user: user,
      })
    );
    props.socket.close();
    logout(dispatch);
  };
  const topClass =
    props.hide === ""
      ? "d-flex flex-row justify-content-between p-3 adiv text-white"
      : "d-flex flex-row justify-content-center p-3 adiv text-white";
  const leftClass = "fas fa-chevron-left " + props.hide;
  const rightClass = "fas fa-power-off" + props.hide + " logout-button";
  const [logouContainer, setLogoutContainer] = useState("hidden");
  return (
    <div>
      <div className={topClass}>
        <i className={leftClass} />
        <span className="pb-3 text-center">Chat Bot</span>
        <i
          className={rightClass}
          onClick={() => setLogoutContainer("logout-container")}
        />
      </div>
      <div className={logouContainer}>
        <span>apakah yakin mau keluar?</span>
        <button onClick={handleClick}>yes</button>
        <button onClick={() => setLogoutContainer("hidden")}>no</button>
      </div>
    </div>
  );
};

export default Header;
