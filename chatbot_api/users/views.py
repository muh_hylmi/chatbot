from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from .serializers import UserSerializer
from .models import User
import jwt, datetime
import re
import random
# import long_responses as long


# Create your views here.
class RegisterView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class LoginView(APIView):
    def post(self, request):
        email = request.data['email']
        password = request.data['password']

        user = User.objects.filter(email=email).first()

        if user is None:
            raise AuthenticationFailed('User not found!')

        if not user.check_password(password):
            raise AuthenticationFailed('Incorrect password!')

        payload = {
            'email': user.email,
            'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=24),
            'iat': datetime.datetime.utcnow()
        }

        token = jwt.encode(payload, 'secret', algorithm='HS256')

        response = Response()

        response.data = {
            'userID': user.id,
            'name': user.name,
            'email': user.email,
            'jwt': token
        }
        return response


class UserView(APIView):

    def get(self, request):
        token = request.COOKIES.get('jwt')

        if not token:
            raise AuthenticationFailed('Unauthenticated!')

        try:
            payload = jwt.decode(token, 'secret', algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')

        user = User.objects.filter(email=payload['email']).first()
        serializer = UserSerializer(user)
        return Response(serializer.data)


class LogoutView(APIView):
    def post(self, request):
        response = Response()
        response.delete_cookie('jwt')
        response.data = {
            'message': 'success'
        }
        return response


class ChatView(APIView):
    def get(self, request, chat):
        data = [
            {'bot_response': 'Hello!', 'list_of_words': [
                'hello', 'hi', 'hey', 'sup', 'heyo'], 'single_response':True, 'required_words':[]},
            {'bot_response': 'See you!', 'list_of_words': [
                'bye', 'goodbye'], 'single_response':True, 'required_words':[]},
            {'bot_response': 'I\'m doing fine, and you?', 'list_of_words': [
                'how', 'are', 'you', 'doing'], 'single_response':False, 'required_words':['how', 'are']},
            {'bot_response': 'I created two days ago', 'list_of_words': [
                'how', 'old', 'are', 'you'], 'single_response':False, 'required_words':['how', 'old']},
            {'bot_response': 'You\'re welcome!', 'list_of_words': [
                'thank', 'thanks'], 'single_response':True, 'required_words':['how', 'are']},
            {'bot_response': 'Thank you!', 'list_of_words': [
                'i', 'love', 'code', 'palace'], 'single_response':False, 'required_words':['code', 'palace']},
        ]
        def message_probability(user_message, recognised_words, single_response=False, required_words=[]):
            message_certainty = 0
            has_required_words = True

            # Counts how many words are present in each predefined message
            for word in user_message:
                if word in recognised_words:
                    message_certainty += 1

            # Calculates the percent of recognised words in a user message
            percentage = float(message_certainty) / float(len(recognised_words))

            # Checks that the required words are in the string
            for word in required_words:
                if word not in user_message:
                    has_required_words = False
                    break

            # Must either have the required words, or be a single response
            if has_required_words or single_response:
                return int(percentage * 100)
            else:
                return 0


        def unknown():
            response = ["Could you please re-phrase that? ",
                        "...",
                        "Sounds about right.",
                        "What does that mean?"][
                random.randrange(4)]
            return response


        def check_all_messages(message):
            highest_prob_list = {}

            # Simplifies response creation / adds it to the dict
            def response(bot_response, list_of_words, single_response=False, required_words=[]):
                nonlocal highest_prob_list
                highest_prob_list[bot_response] = message_probability(
                    message, list_of_words, single_response, required_words)

            # Responses -------------------------------------------------------------------------------------------------------
            for i in data:
                response(i['bot_response'], i['list_of_words'],
                        i['single_response'], i['required_words'])
    

            best_match = max(highest_prob_list, key=highest_prob_list.get)
            # print(f'Best match = {best_match} | Score: {highest_prob_list[best_match]}')

            return unknown() if highest_prob_list[best_match] < 1 else best_match


        # Used to get the response
        def get_response(user_input):
            split_message = re.split(r'\s+|[,;?!.-]\s*', user_input.lower())
            response = check_all_messages(split_message)
            return response

        chat = get_response(chat)
        response = Response()
        response.data = {'bot': chat}
        return response
    
