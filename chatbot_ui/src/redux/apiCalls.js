import { loginFailure, loginStart, loginSuccess, logoutUser } from "./userRedux";
import { publicRequest } from "../requestMethods";

export const login = async (dispatch, user) => {
    dispatch(loginStart());
    try {
        const res = await publicRequest.post("/login", user);
        dispatch(loginSuccess(res.data));
        return true;
    } catch (err) {
        dispatch(loginFailure());
        return false;
    }
};

export const logout = async (dispatch) => {
    dispatch(logoutUser());
}
