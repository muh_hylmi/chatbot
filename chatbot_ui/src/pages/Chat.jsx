import React, { useEffect, useState } from "react";
import Chatdata from "../component/Chatdata";
import Header from "../component/Header";
import "./chat.css";
import { useSelector } from "react-redux";
import { useIdleTimer } from "react-idle-timer";
import { logout } from "../redux/apiCalls";
import { useDispatch } from "react-redux";

const Chat = () => {
  const [socket, setSocket] = useState();
  const [message, setMessage] = useState("");
  const [listMessages, setListMessages] = useState([]);
  const [idlerShow, setIdlerShow] = useState("hidden");
  const [logouContainer, setLogoutContainer] = useState("hidden");
  const dispatch = useDispatch();
  const token = useSelector((state) => state.user.currentUser?.jwt);
  const user = useSelector((state) => state.user.currentUser?.name);
  useEffect(() => {
    const socket = new WebSocket("ws://localhost:8000/ws/some_url/");
    socket.onopen = function (event) {
      socket.send(
        JSON.stringify({
          type: "connect",
          user: user,
        })
      );
    };
    setSocket(socket);
  }, [user]);

  const handleIdler = () => {
    setListMessages([]);
    setIdlerShow("hidden");
    setLogoutContainer("logout-container");
  };
  const handleOnIdle = (event) => {
    setIdlerShow("container idle-container");
  };

  useIdleTimer({
    timeout: 1000 * 15,
    onIdle: handleOnIdle,
    events: ["keydown", "keypress", "keyup"],
  });

  const handleClick = (e) => {
    setListMessages([...listMessages, { isBot: false, message: message }]);
    socket.send(
      JSON.stringify({
        type: "chat",
        token: token,
        message: message,
      })
    );
    setMessage("");
  };

  const handleLogout = () => {
    socket.send(
      JSON.stringify({
        type: "dissconnect",
        user: user,
      })
    );
    socket.close();
    logout(dispatch);
  };
  if (socket) {
    socket.onmessage = function (event) {
      var data = JSON.parse(event.data);
      if (data !== "Unauthenticated") {
        if (data.token === token) {
          setListMessages([...listMessages, data.response]);
        }
      }
    };
  }
  return (
    <div>
      <div className="container d-flex justify-content-center">
        <div className="card mt-5">
          <Header hide="" socket={socket} />
          <div className={idlerShow}>
            <span>apakah masih ada yang bisa saya bantu?</span>
            <button className="ya" onClick={() => setIdlerShow("hidden")}>
              Ya
            </button>
            <button className="tidak" onClick={handleIdler}>
              Tidak
            </button>
          </div>
          <div className={logouContainer}>
            <span>apakah yakin mau keluar?</span>
            <button onClick={handleLogout}>yes</button>
            <button onClick={() => setLogoutContainer("hidden")}>no</button>
          </div>

          <div className="d-flex flex-row p-3 ml-auto">
            <div className="bg-white mr-2 p-3">
              <span className="text-muted">
                Hello <b>{user}</b>, Botton disini! ada yang bisa saya bantu?
              </span>
            </div>
            <img
              src="https://img.icons8.com/color/48/000000/circled-user-male-skin-type-7.png"
              width={50}
              height={50}
              alt=""
            />
          </div>
          {listMessages.map((list) => {
            return <Chatdata isBot={list.isBot} message={list.message} />;
          })}
          <div className="form-group px-3 d-flex align-items-center mt-auto">
            <textarea
              className="form-control mr-3"
              rows={5}
              placeholder="Type your message"
              value={message}
              onChange={(e) => setMessage(e.target.value)}
            />
            <button className="send-message" onClick={handleClick}>
              <i className="fas fa-paper-plane" />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Chat;
